# Dryades

Some simple animations I've made with CSS and a minimal amount of HTML markup.

## Usage
Include the stylesheet for the animation you want to use

```html
<link rel="stylesheet" href="css/loader_1.css">
```

Then include the spinner in your HTML markup

```html
<div class="loader-1"></div>
```

## Animations

The list of animations available

| Class name | Description | Stylesheet |
| -----------| ----------- | ---------- |
| .loader-1 | Spinning circle split in two colours | css/loader_1.css |
| .loader-2 | Spinning circle split in two colours that changes size | css/loader_2.css |
| .loader-3 | Trailing circle spinner | css/loader_3.css |
| .loader-4 | Bouncing ball | css/loader_4.css |
| .loader-5 | Spinning square that changes size | css/loader_5.css |
| .loader-6 | Raindrop circle that changes size and transparency | css/loader_6.css |
| .loader-7 | Three raindrops that change size and transparency | css/loader_7.css |

## Contributions

Contributions are welcome, feel free to submit a merge request :)

## License

Licensed under MIT License